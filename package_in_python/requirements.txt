requests~=2.25.1
pytz~=2021.1
numpy~=1.20.2
opencv-contrib-python~=4.5.2.52