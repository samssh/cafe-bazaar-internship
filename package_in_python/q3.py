import numpy as np
import cv2 as cv


def main():
    result_size = 500
    im = np.zeros((result_size, result_size, 3), 'uint8')
    step_size = result_size // 10
    for i in range(10):
        im[:, i * step_size:i * step_size + step_size // 2, 2] = 255
        im[:, i * step_size + step_size // 2:(i + 1) * step_size, 0] = 255
    cv.imwrite('result.png', im)


if __name__ == '__main__':
    main()
