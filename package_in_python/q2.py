import datetime
import pytz


def get_nearest_country(target):
    now = datetime.datetime.now()
    dif = None
    result = None
    for country_code in pytz.country_timezones:
        timezones = pytz.country_timezones[country_code]
        for timezone in timezones:
            tz = pytz.timezone(timezone)
            loc = tz.localize(target)
            tt = now.astimezone(tz)
            if dif is None:
                dif = abs(tt - loc)
                result = country_code
            if dif >= abs(tt - loc):
                result = country_code
                dif = abs(tt - loc)
    return result


if __name__ == '__main__':
    # test
    get_nearest_country(datetime.datetime.now())
