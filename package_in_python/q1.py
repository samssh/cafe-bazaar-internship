import argparse
import time

import requests


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--url', type=str, default='https://www.wikipedia.org/', help='URL of vclass')
    args = parser.parse_args()
    res = requests.get(args.url)
    while True:
        time.sleep(10)
        res2 = requests.get(args.url)
        if res.content != res2.content:
            print('site changed')
            res = res2
        # else:
        #     print('not changed')


if __name__ == '__main__':
    main()
