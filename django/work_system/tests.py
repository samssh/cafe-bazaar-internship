from django.test import TestCase
from django.test import utils
from django.contrib.auth.models import User
from . import models
from django import urls
from django.utils import timezone
from datetime import timedelta


def create_task(number, code):
    user = User.objects.create_user(f'test{code}', f'test{code}@test.com', 'password')
    for i in range(number):
        models.Task.objects.create(title=str(i), value=i, timeEstimation=timedelta(days=i),
                                   employer=user, description="desc" + str(i), date_created=timezone.now())
        pass
    pass


# Create your tests here.
class TaskView(TestCase):

    def setUp(self) -> None:
        create_task(10, 1)
        pass

    def test_session1(self):
        response1 = self.client.get(urls.reverse('work_system:home') + '?page=2')
        self.assertEqual(response1.status_code, 200)
        response2 = self.client.get(urls.reverse('work_system:home'))
        self.assertEqual(response2.status_code, 200)
        self.assertEqual(response2.context['page_obj'].number, 2)

        response3 = self.client.get(urls.reverse('work_system:home') + '?page=3')
        self.assertEqual(response3.status_code, 200)
        response4 = self.client.get(urls.reverse('work_system:home'))
        self.assertEqual(response4.status_code, 200)
        self.assertEqual(response4.context['page_obj'].number, 3)
        pass

    def test_detail(self):
        res1 = self.client.get(urls.reverse('work_system:task_detail', args=(5,)))
        self.assertEqual(res1.status_code, 200)
        self.assertEqual(res1.context['task'].pk, 5)
        res2 = self.client.get(urls.reverse('work_system:task_detail', args=(10000,)))
        self.assertEqual(res2.status_code, 404)
        pass

    pass

    pass


class Login(TestCase):
    def test_user_notfound(self) -> None:
        res = self.client.post(urls.reverse('work_system:email_login'), {'email': "not_exist@gmail.com"})
        self.assertEqual(res.status_code, 404)
        pass
