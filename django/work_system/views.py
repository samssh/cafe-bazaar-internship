import secrets
from datetime import timedelta

import redis
from django import shortcuts
from django.conf import settings
from django.contrib import auth
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core.mail import send_mail
from django.core.paginator import Paginator

from . import models

redis_connection = redis.Redis(port=settings.REDIS_PORT, decode_responses=True)


# Create your views here.
def home(request):
    task_list = models.Task.objects.all()
    paginator = Paginator(task_list, 2)
    form = models.SignupForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            user = form.save()
            print(user.isEmployee)
            if user.isEmployee:
                content_type = ContentType.objects.get_for_model(models.Task)
                permission = Permission.objects.get(codename='can_create_task', content_type=content_type)
            else:
                content_type = ContentType.objects.get_for_model(models.Task)
                permission = Permission.objects.get(codename='can_reserve_task', content_type=content_type)
            user.user_permissions.add(permission)
            return shortcuts.redirect('work_system:account_created')
    if 'page' in request.GET:
        page_number = request.GET.get('page')
        request.session['page'] = page_number
    elif 'page' in request.session:
        page_number = request.session['page']
    else:
        page_number = 1
    page_obj = paginator.get_page(page_number)
    return shortcuts.render(request, 'index.html', {'page_obj': page_obj, 'signup_form': form})


def account_created(request):
    return shortcuts.render(request, 'account_created.html')


def task_detail(request, task_id):
    task = shortcuts.get_object_or_404(models.Task, pk=task_id)
    return shortcuts.render(request, 'details.html', {'task': task})


def email_login(request):
    token = secrets.token_urlsafe(32)
    email: str = request.POST['email']
    user = shortcuts.get_object_or_404(models.User, email=email.lower())
    redis_connection.set(token, user.username, ex=3600)
    send_mail(
        subject='login',
        message='goto this link\n' +
                request.build_absolute_uri(shortcuts.reverse('work_system:email_login_token', args=(token,))),
        from_email=None,
        recipient_list=[email],
    )
    return shortcuts.redirect('work_system:email_sent', email)


def email_login_token(request, token):
    username = redis_connection.get(token)
    if username is None:
        return shortcuts.Http404()
    redis_connection.delete(token)
    auth.login(request, shortcuts.get_object_or_404(models.User, username=username))
    return shortcuts.redirect('work_system:home')


def email_sent(request, email):
    return shortcuts.render(request, 'send_mail.html', {'email': email})


def logout(request):
    auth.logout(request)
    return shortcuts.redirect('work_system:home')


@permission_required('work_system.can_create_task', login_url='/no_permission')
def create_task(request):
    form = models.TaskForm(request.POST or None)
    if form.is_valid():
        task = form.save(commit=False)
        task.employer = request.user
        task.timeEstimation = timedelta(form.cleaned_data['timeEstimationDay'], form.cleaned_data['timeEstimationHour'])
        task.save()
        return shortcuts.redirect('work_system:create_task_success')
    return shortcuts.render(request, 'new_task.html', {'form': form})


def create_task_success(request):
    return shortcuts.render(request, 'task_created.html')


def no_permission(request):
    return shortcuts.render(request, 'no_permission.html')


@permission_required('work_system.can_reserve_task', login_url='/no_permission')
def reserve_task(request, task_id):
    task = shortcuts.get_object_or_404(models.Task, pk=task_id)
    if not task.worker:
        task.worker = request.user
        task.save()
        return shortcuts.redirect('work_system:task_reserved')
    else:
        return shortcuts.redirect('work_system:task_full')
    pass


def task_reserved(request):
    return shortcuts.render(request, 'task_reserved.html')


def task_full(request):
    return shortcuts.render(request, 'task_full.html')
