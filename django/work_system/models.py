from django.db import models
from django import forms
from django.contrib.auth import models as auth_model
from django.utils.timezone import now


# Create your models here.
class User(auth_model.AbstractUser):
    isEmployee = models.BooleanField(default=False)
    pass


class Task(models.Model):
    title = models.CharField(max_length=100)
    value = models.IntegerField()
    timeEstimation = models.DurationField()
    employer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tasks')
    worker = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_query_name='taskTodo')
    description = models.TextField()
    dateCreated = models.DateTimeField(default=now)

    class Meta:
        ordering = ["dateCreated"]
        permissions = [('can_create_task', 'can create task'), ('can_reserve_task', 'can reserve task')]
        pass

    pass


class EmailToken(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=64, unique=True)


class SignupForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    password_rpt = forms.CharField(widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].required = True

    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'isEmployee']
        pass

    def clean(self):
        cleaned_data = super(SignupForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("password_rpt")

        if password != confirm_password:
            raise forms.ValidationError('password and confirm_password does not match')
        pass

    def clean_email(self):
        data = self.cleaned_data['email']
        if User.objects.filter(email=data).exists():
            raise forms.ValidationError("This email already used")
        return data

    pass


class TaskForm(forms.ModelForm):
    timeEstimationDay = forms.IntegerField(min_value=0)
    timeEstimationHour = forms.IntegerField(min_value=0, max_value=23)

    class Meta:
        model = Task
        fields = ['title', 'value', 'description']
        pass

    def clean(self):
        cleaned_data = super(TaskForm, self).clean()
        day = cleaned_data.get("timeEstimationDay", 0)
        value = cleaned_data.get("value", 0)
        if day <= 3 and value > 30000:
            raise forms.ValidationError('حداکثر قیمت کار‌های با زمان ۳ روز ۳۰۰۰۰ تومان می باشد')
        pass

    def clean_value(self):
        data = self.cleaned_data['value']
        if data < 1000 or data > 50000:
            raise forms.ValidationError("قیمت باید بین ۱۰۰۰ تا ۵۰۰۰۰ تومان باشد")
        return data
