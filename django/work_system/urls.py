from django.urls import path
from . import views

app_name = 'work_system'
urlpatterns = [
    path('', views.home, name='home'),
    path('index.html', views.home, name='home'),
    path('task/<int:task_id>/detail', views.task_detail, name='task_detail'),
    path('login/email', views.email_login, name='email_login'),
    path('login/email/<str:token>', views.email_login_token, name='email_login_token'),
    path('login/email_sent/<str:email>', views.email_sent, name='email_sent'),
    path('logout', views.logout, name='logout'),
    path('account_created', views.account_created, name='account_created'),
    path('create_task', views.create_task, name='create_task'),
    path('create_task/succses', views.create_task_success, name='create_task_success'),
    path('no_permission', views.no_permission, name='no_permission'),
    path('reserve_task/<int:task_id>', views.reserve_task, name='reserve_task'),
    path('task_reserved', views.task_reserved, name='task_reserved'),
    path('task_full', views.task_full, name='task_full'),
]
