import redis

redisClient = redis.StrictRedis(host='localhost', port=6381, db=0, decode_responses=True)
players = "Players"


def add_record(name, score):
    redisClient.zadd(players, {name: score})
    pass


def get_top_ten():
    return redisClient.zrangebyscore(players, '-inf', '+inf', start=0, num=2, withscores=True)
    pass

