from rest_framework import generics
from rest_framework import permissions, status
from rest_framework import views
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.request import Request
from rest_framework.response import Response

from . import models
from . import permissions as my_perm
from . import serializers


# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    queryset = models.User.objects.all()
    serializer_class = serializers.UserSerializers
    permission_classes = [my_perm.UserPermission,
                          my_perm.CheckJWTUserPermission, ]
    token = ''

    def perform_create(self, serializer):
        user = serializer.save()
        token = Token.objects.create(user=user)
        self.token = token.key
        return

    def get_success_headers(self, data):
        if self.token != '':
            res = super().get_success_headers(data)
            res['token'] = self.token
            return res
        return super().get_success_headers(data)

    pass


class PostViewSet(viewsets.ModelViewSet):
    queryset = models.Post.objects.all()
    serializer_class = serializers.PostSerializers
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          my_perm.IsOwnerOrReadOnly,
                          my_perm.CheckJWTPostPermission, ]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
        return

    pass


class CommentViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.CommentSerializers
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          my_perm.IsOwnerOrReadOnly,
                          my_perm.CheckJWTCommentPermission]

    def get_queryset(self):
        post_id = self.kwargs['post_id']
        return models.Comment.objects.filter(post_id=post_id)

    def perform_create(self, serializer):
        post = models.Post.objects.get(pk=self.kwargs['post_id'])
        serializer.save(user=self.request.user, post=post)

    pass


class CommentGenericView(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Comment.objects.all()
    serializer_class = serializers.CommentSerializers
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          my_perm.IsOwnerOrReadOnly,
                          my_perm.CheckJWTCommentPermission]
    pass


class CommentList(generics.ListAPIView):
    queryset = models.Comment.objects.all()
    serializer_class = serializers.CommentSerializers
    pass


class PostByUserList(generics.ListAPIView):
    serializer_class = serializers.PostSerializers

    def get_queryset(self):
        user_id = self.kwargs['user_id']
        return models.Post.objects.filter(post_id=user_id)

    pass


class PostWithCommentList(generics.ListAPIView):
    serializer_class = serializers.PostWithCommentSerializer

    def get_queryset(self):
        return models.Post.objects.order_by('last_change').all()

    pass


class JWTView(views.APIView):
    permission_classes = [my_perm.IsAuthenticatedByAuth]

    @staticmethod
    def post(request: Request):
        perms = serializers.JWTPermissionSerializer(data=request.data)
        if perms.is_valid():
            token_serializer = serializers.JWTObtainPairSerializer(request.user, perms.validated_data, data={})
            if token_serializer.is_valid():
                return Response(token_serializer.validated_data, status=status.HTTP_201_CREATED)
            return Response(token_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(perms.errors, status=status.HTTP_400_BAD_REQUEST)

    pass
