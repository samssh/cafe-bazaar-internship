import rest_framework.permissions as rest_perm
from rest_framework.authentication import TokenAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication


class UserPermission(rest_perm.BasePermission):

    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        perm1 = rest_perm.IsAuthenticatedOrReadOnly()
        return perm1.has_permission(request, view)

    def has_object_permission(self, request, view, obj):
        if request.method in rest_perm.SAFE_METHODS:
            return True
        return obj == request.user

    pass


class IsOwnerOrReadOnly(rest_perm.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in rest_perm.SAFE_METHODS:
            return True
        return obj.user == request.user

    pass


class IsAuthenticatedByAuth(rest_perm.BasePermission):

    def has_permission(self, request, view):
        perm1 = rest_perm.IsAuthenticatedOrReadOnly()
        if not perm1.has_permission(request, view):
            return False
        return isinstance(request._authenticator, TokenAuthentication)

    pass


class CheckJWTPostPermission(rest_perm.BasePermission):
    def has_permission(self, request, view):
        if request.method in rest_perm.SAFE_METHODS:
            return True
        perm1 = rest_perm.IsAuthenticatedOrReadOnly()
        if not perm1.has_permission(request, view):
            return False
        if not isinstance(request._authenticator, JWTAuthentication):
            return True
        token = request.auth
        if request.method == 'POST':
            return token['create_post']
        if request.method == 'PUT' or request.method == 'PATCH':
            return token['update_post']
        if request.method == 'DELETE':
            return token['delete_post']
        return False

    pass


class CheckJWTCommentPermission(rest_perm.BasePermission):
    def has_permission(self, request, view):
        if request.method in rest_perm.SAFE_METHODS:
            return True
        perm1 = rest_perm.IsAuthenticatedOrReadOnly()
        if not perm1.has_permission(request, view):
            return False
        if not isinstance(request._authenticator, JWTAuthentication):
            return True
        token = request.auth
        if request.method == 'POST':
            return token['create_comment']
        if request.method == 'PUT' or request.method == 'PATCH':
            return token['update_comment']
        if request.method == 'DELETE':
            return token['delete_comment']
        return False

    pass


class CheckJWTUserPermission(rest_perm.BasePermission):
    def has_permission(self, request, view):
        if request.method in rest_perm.SAFE_METHODS:
            return True
        perm1 = rest_perm.IsAuthenticatedOrReadOnly()
        if not perm1.has_permission(request, view):
            return False
        if not isinstance(request._authenticator, JWTAuthentication):
            return True
        token = request.auth
        if request.method == 'PUT' or request.method == 'PATCH':
            return token['update_user']
        if request.method == 'DELETE':
            return token['delete_user']
        return False

    pass
