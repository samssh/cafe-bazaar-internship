from django.urls import path
from rest_framework.routers import DefaultRouter
from . import views
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
app_name = 'twitter'
urlpatterns = [
    path('comments/<int:pk>/', views.CommentGenericView.as_view(), name='comment-by-pk'),
    path('comments/', views.CommentList.as_view(), name='comment-by-pk-list'),
    path('users/<int:user_id>/posts/', views.PostByUserList.as_view(), name='post-by-user-list'),
    path('post_with_comment/', views.PostWithCommentList.as_view(), name='post_with_comment'),
    path('jwtoken/', views.JWTView.as_view(), name='token_obtain_pair'),
]

router = DefaultRouter()
router.register(r'users', views.UserViewSet, basename='user')
router.register(r'posts', views.PostViewSet, basename='post')
router.register(r'posts/(?P<post_id>[0-9]+)/comments', views.CommentViewSet, basename='comment')
urlpatterns += router.urls
