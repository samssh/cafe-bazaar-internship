from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken

from . import models


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = ['pk', 'username', 'first_name']
        read_only_fields = ['pk']
        pass

    pass


class PostSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Post
        fields = ['pk', 'content']
        read_only_fields = ['pk']
        pass

    pass


class CommentSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Comment
        fields = ['pk', 'content', 'post_id', 'user_id']
        read_only_fields = ['pk', 'post_id', 'user_id']
        pass

    pass


class PostWithCommentSerializer(serializers.ModelSerializer):
    comments = CommentSerializers(many=True)

    class Meta:
        model = models.Post
        fields = ['pk', 'content', 'comments']
        pass

    pass


class JWTPermissionSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        return validated_data

    def create(self, validated_data):
        return validated_data

    create_post = serializers.BooleanField(default=False)
    update_post = serializers.BooleanField(default=False)
    delete_post = serializers.BooleanField(default=False)
    create_comment = serializers.BooleanField(default=False)
    update_comment = serializers.BooleanField(default=False)
    delete_comment = serializers.BooleanField(default=False)
    update_user = serializers.BooleanField(default=False)
    delete_user = serializers.BooleanField(default=False)


class JWTObtainPairSerializer(serializers.Serializer):
    def __init__(self, user, jwt_permissions, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.jwt_permissions = jwt_permissions

    def update(self, instance, validated_data):
        return validated_data

    def create(self, validated_data):
        return validated_data

    def validate(self, attrs):
        data = super().validate(attrs)

        refresh = self.create_token(self.user, self.jwt_permissions)

        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        return data

    @classmethod
    def create_token(cls, user, jwt_permissions: dict):
        token = RefreshToken.for_user(user)
        for key, value in jwt_permissions.items():
            token[key] = value
        return token
