create table if not exists friendship
(
	user_id int not null
		constraint friend_ship_user_id_fk
			references "user"
				on delete cascade,
	friend_id int not null
		constraint friend_ship_user_id_fk_2
			references "user"
				on delete cascade
);