SELECT u.name usernames, array_agg(u2.name) friend_names
FROM "user" u
         JOIN friendship f on u.id = f.user_id
         JOIN "user" u2 ON f.friend_id = u2.id
WHERE age(u.birthday) > '18 years'
GROUP BY u.name;