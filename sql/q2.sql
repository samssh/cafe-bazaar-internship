CREATE TABLE IF NOT EXISTS task
(
    title         VARCHAR not null,
    expected_time INTERVAL,
    value         integer,
    deadline      date    not null,
    user_id       integer not null
);