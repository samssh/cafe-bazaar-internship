import psycopg2
from faker import Faker


def main():
    try:
        connection = psycopg2.connect(user="internship_user",
                                      password="password1",
                                      host="127.0.0.1",
                                      port="5432",
                                      database="internship")

        cursor = connection.cursor()
        create_table_query = '''CREATE TABLE IF NOT EXISTS "user"(
                                id serial not null,
                                name VARCHAR not null,
                                email VARCHAR not null,
                                phone_number VARCHAR(30) not null,
                                birthday date not null);
                                create unique index user_id_unique_index on "user" (id);
                                alter table "user" add constraint user_pk primary key (id);
                '''
        cursor.execute(create_table_query)
        connection.commit()
        faker = Faker()
        for _ in range(1000):
            create_user_query = f'''INSERT INTO "user" (
                                    name,
                                    email,
                                    phone_number,
                                    birthday)
                                    VALUES (
                                    '{faker.name()}',
                                    '{faker.email()}',
                                    '{faker.phone_number()}',
                                    '{faker.date_of_birth()}')
                             '''
            cursor.execute(create_user_query)
            connection.commit()
    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL", error)
    finally:
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")


if __name__ == '__main__':
    main()
