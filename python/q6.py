import json


def main():
    i, o = input().split()
    f = open(i, 'r')
    objs = []
    for l1 in f.readlines():
        objs.append(json.loads(l1))
    objs = sorted(objs, key=lambda obj1: obj1['id'])
    f = open(o, 'w')
    for obj in objs:
        f.write(json.dumps(obj) + '\n')
    pass


if __name__ == '__main__':
    main()
