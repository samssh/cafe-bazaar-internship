from unidecode import unidecode


def normalize_number(number):
    return unidecode(number)


print(normalize_number('۱۲۳'))
