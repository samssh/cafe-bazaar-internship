import sys


def main():
    n = -1
    if len(sys.argv) > 1:
        if sys.argv[0] == '--help':
            print('to creat user write:CREATE USER [USER NAME]')
            print('to creat task write:CREATE TASK [TASK NAME]')
            print('to assign task for a user write:ASSIGN [TASK NAME] [USER NAME]')
            print('to see user task list write:LIST USER [USER NAME]')
            print('to see task user list write:LIST TASK [TASK NAME]')
            print('to exit write:EXIT')
        elif sys.argv[0] == '-n' and len(sys.argv) > 2:
            n = int(sys.argv[1])
    tasks = dict()
    users = dict()
    while n != 0:
        i = input().split()
        if len(i) == 1 and i[0] == 'EXIT':
            break
        if len(i) != 3:
            print('command invalid')
            continue
        if i[0] == 'CREATE':
            if i[1] == 'USER':
                if i[2] in users:
                    print('user exists')
                else:
                    users[i[2]] = []
            elif i[1] == 'TASK':
                if i[2] in tasks:
                    print('task exists')
                else:
                    tasks[i[2]] = []
            else:
                print('command not valid')
        elif i[0] == 'ASSIGN':
            print(tasks)
            print(users)
            print(i)
            if (not i[1] in tasks) or (not i[2] in users):
                print('task or user not exist')
            else:
                tasks[i[1]].append(i[2])
                users[i[2]].append(i[1])
        elif i[0] == 'LIST':
            if i[1] == 'USER':
                if not i[2] in users:
                    print('user not exists')
                else:
                    print(users[i[2]])
            elif i[1] == 'TASK':
                if not i[2] in tasks:
                    print('task not exists')
                else:
                    print(tasks[i[2]])
            else:
                print('command not valid')
        else:
            print('command not valid')
        n -= 1
    pass


if __name__ == '__main__':
    main()
