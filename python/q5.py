import os


def list_files(path):
    index = open(os.path.join(path, 'index.txt'), 'w')
    for root, d_names, f_names in os.walk(path):
        for f in f_names:
            if os.path.join(path, 'index.txt') == os.path.join(root, f):
                continue
            index.write(os.path.join(root, f) + '\n')
    index.close()


list_files('.')
