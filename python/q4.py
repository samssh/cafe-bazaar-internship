def number_splitter(num):
    result = ''
    while num > 0:
        if num > 999:
            result = ','+str(num % 1000) + result
        else:
            result = str(num % 1000) + result
        num = num // 1000
    return result


print(number_splitter(1231546848979))
