import re


def find_time_in_subtitle(path):
    f = open(path, 'r')
    h, m, s, ms = 0, 0, 0, 0
    pattern = "[0-9][0-9]:[0-9][0-9]:[0-9][0-9],[0-9][0-9][0-9] --> [0-9][0-9]:[0-9][0-9]:[0-9][0-9],[0-9][0-9][0-9]"
    for line in f.readlines():
        match = re.search(pattern, line)
        print(match)
        if match is not None:
            numbers = [int(n) for n in re.findall(r'\d+', match.string)]
            print(numbers)
            h += numbers[4] - numbers[0]
            m += numbers[5] - numbers[1]
            s += numbers[6] - numbers[2]
            ms += numbers[7] - numbers[3]
    s += ms // 1000
    ms = ms % 1000
    m += s // 60
    s = s % 60
    h += m // 60
    m = m % 60
    return h, m, s, ms


print(find_time_in_subtitle('sample.srt'))
