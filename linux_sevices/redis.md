<div dir="auto">
برای نصب ردیس از کامند زیر استفاده می‌کنیم.
</div>


```
sudo apt install redis-server
```

<div dir="auto">
برای تنظیم کردن پورت ردیس فایل 
/etc/redis/redis.conf
باز می‌کنیم و پورت جدید را تنظیم می‌کنیم.
</div>

<div dir="auto">
همچنین عبارت زیر را به فایل کانفیگ اضافه می‌کنیم.
</div>

```
supervised systemd
```

<div dir="auto">
با کامند زیر می‌توانیم ردیس را بالا بیاوریم.
</div>

```shell
sudo service redis-server start
```
