import json
import os

import flask
from flask import Flask
from markupsafe import escape
import secrets
import datetime

token_life = datetime.timedelta(minutes=10)
app = Flask(__name__)
app.config.update(
    SECRET_KEY=secrets.token_urlsafe(32)
)


def read_all_lines(path):
    f = open(path, 'r')
    all_lines = ''
    for l1 in f.readlines():
        all_lines = all_lines + '\n' + l1
    f.close()
    return all_lines


def save_to_file(path, content):
    f = open(path, 'w')
    f.write(content)
    f.flush()
    f.close()


def read_user_data():
    return json.loads(read_all_lines('data/users.json'), object_hook=date_hook)


def read_tokens():
    return json.loads(read_all_lines('data/tokens.json'), object_hook=date_hook)


def read_content_data():
    return json.loads(read_all_lines('data/content.json'), object_hook=date_hook)


def save_users(users):
    save_to_file('data/users.json', json.dumps(users, default=str))


def save_tokens(tokens):
    save_to_file('data/tokens.json', json.dumps(tokens, default=str))


def save_contents(contents):
    save_to_file('data/content.json', json.dumps(contents, default=str))


def save_text(content, content_id):
    save_to_file(f'texts/{content_id}.txt', content)


def load_text(content_id):
    return read_all_lines(f'texts/{content_id}.txt')


def delete_text_file(content_id):
    if os.path.exists(f'texts/{content_id}.txt'):
        os.remove(f'texts/{content_id}.txt')


def date_hook(json_dict):
    for (key, value) in json_dict.items():
        try:
            json_dict[key] = datetime.datetime.strptime(value, "%Y-%m-%d %H:%M:%S.%f")
        except Exception as e:
            pass
    return json_dict


def valid_login(username, password):
    users = read_user_data()
    if username not in users:
        return 'username not exist'
    if users[username]['password'] != password:
        print()
        return 'password is wrong!!'
    return None


def delete_expired_tokens(tokens: dict):
    new_tokens = dict()
    for item in tokens.items():
        if item[1]['expire_time'] >= datetime.datetime.now():
            new_tokens[item[0]] = item[1]
    return new_tokens


def delete_token(token, tokens):
    if token in tokens:
        del tokens[token]
    save_tokens(tokens)


def log_the_user_in(username):
    token = secrets.token_urlsafe(32)
    tokens = read_tokens()
    temp = {
        "username": username,
        "expire_time": datetime.datetime.now() + token_life
    }
    tokens[token] = temp
    tokens = delete_expired_tokens(tokens)
    save_tokens(tokens)
    res = flask.redirect('/home')
    res.set_cookie('token', token, max_age=token_life)
    return res


def create_user(name, username, password):
    users = read_user_data()
    if username in users:
        return 'username exists', None
    temp = dict()
    temp['name'] = name
    temp['password'] = password
    users[username] = temp
    save_users(users)
    return None, log_the_user_in(username)


def login_with_token(token):
    tokens = read_tokens()
    if token in tokens:
        if tokens[token]['expire_time'] > datetime.datetime.now():
            return tokens[token]['username']
        else:
            delete_token(token, tokens)
            return None
    else:
        return None


def check_token():
    res = None
    username = None
    if not flask.request.cookies.get('token'):
        res = flask.redirect('/login')
    else:
        token = flask.request.cookies.get('token')
        username = login_with_token(token)
        if username is None:
            res = flask.redirect('/login')
    return res, username


def get_user(username):
    users = read_user_data()
    return users[username]


@app.route('/login', methods=['POST', 'GET'])
def login():
    res, username = check_token()
    if username is not None:
        return flask.redirect('/home')
    error = None
    if flask.request.method == 'POST':
        error = valid_login(flask.request.form['username'], flask.request.form['password'])
        if error is None:
            return log_the_user_in(flask.request.form['username'])
    return flask.render_template('login.html', error=error)


@app.route('/register', methods=['POST', 'GET'])
def register():
    res, username = check_token()
    if username is not None:
        return flask.redirect('/home')
    error = None
    if flask.request.method == 'POST':
        name = flask.request.form['name']
        username = flask.request.form['username']
        password = flask.request.form['password']
        error, response = create_user(name, username, password)
        if error is None:
            return response
    return flask.render_template('register.html', error=error)


@app.route('/favicon.ico')
def favicon():
    return flask.send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico')


@app.route('/')
@app.route('/home')
def home():
    res, username = check_token()
    if username is not None:
        name = get_user(username)['name']
        history = flask.session.get('history', [])[::-1]
        res = flask.render_template('home.html', name=escape(name), history=history)
    return res


@app.route('/logout')
def logout():
    res = flask.redirect('/login')
    if flask.request.cookies.get('token'):
        token = flask.request.cookies.get('token')
        delete_token(token, read_tokens())
        res.set_cookie('token', '', max_age=0)
    return res


@app.route('/add_text', methods=['POST'])
def add_text():
    res, username = check_token()
    if username is not None:
        content = flask.request.form['content']
        contents = read_content_data()
        temp = dict()
        content_id = contents['available_id']
        temp['author'] = username
        contents['available_id'] = content_id + 1
        contents[str(content_id)] = temp
        save_text(content, content_id)
        save_contents(contents)
        res = flask.redirect(f'/show_text/{content_id}')
    return res


@app.route('/show_text/<int:content_id>')
def show_text(content_id):
    res, username = check_token()
    name = None
    if username is not None:
        name = get_user(username)['name']
    contents = read_content_data()
    if str(content_id) not in contents:
        return flask.render_template('404.html'), 404

    author_username = contents[str(content_id)]['author']
    author = get_user(author_username)['name']
    content = load_text(content_id)
    owner = name is not None and username == author_username
    history = flask.session.get('history', [])[::-1]
    return flask.render_template('show_text.html', name=name, author=author, content=content, owner=owner,
                                 content_id=content_id, history=history)


@app.route('/delete_text/<int:content_id>', methods=['POST'])
def delete_text(content_id):
    contents = read_content_data()
    if str(content_id) not in contents:
        return flask.render_template('404.html'), 404
    res, username = check_token()
    if username is None:
        return res
    author_username = contents[str(content_id)]['author']
    if author_username != username:
        return flask.render_template('403.html'), 403
    delete_text_file(content_id)
    del contents[str(content_id)]
    save_contents(contents)
    return flask.redirect('/home')


@app.after_request
def save_response(r):
    if flask.request.method != 'GET':
        return r
    if flask.request.endpoint == 'favicon':
        return r
    history = flask.session.get('history', [])
    if len(history) > 0 and history[-1][0] == flask.request.path:
        return r
    history.append([flask.request.path, flask.request.url])
    flask.session['history'] = history
    return r
