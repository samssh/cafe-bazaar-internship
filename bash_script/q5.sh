STR="$1"
if [ "$1" == '' ]; then
  STR=.
fi
for FILE in "$STR"/*.js; do
  if test -f "$FILE"; then
    if grep -q cafebazaar "$FILE"; then
      echo "$FILE"
    fi
  fi
done
