function dir() {
  TOTAL_FILESIZE=0
  for f in $(find "$1" -name '*')
  do
    FILESIZE=$(stat -c%s "$f")
    TOTAL_FILESIZE=$((TOTAL_FILESIZE + FILESIZE))
  done
  echo $TOTAL_FILESIZE
}

dir "$1"