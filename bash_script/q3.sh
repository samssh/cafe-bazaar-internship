cd "$HOME" || exit

mkdir Desktop/temp

for FILE in Desktop/*
do
  if [ "$FILE" != Desktop/temp ]; then
      mv "$FILE" ./Desktop/temp/
  fi
done

